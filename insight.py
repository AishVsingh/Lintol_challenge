import requests
from bs4 import BeautifulSoup
import spacy
from spacy.lang.en import English
import json
import sys

nlp = English()
nlp.add_pipe('sentencizer')
nlp1 = spacy.load("en_core_web_lg")

 
def get_all_members(contributions):
    members_list=[]
    members={}
    for a in contributions:
        name_link=(a.find("b"))
        if name_link !=None :
            name=(name_link.text).replace(":",'')
            if (name not in members_list):
                    members[name]={
                        "frequency":0,
                        "image":"",
                        "dialogs":[]
                    }
                    members_list.append(name)
    return {"list":members_list,"skeleton":members}

def get_frequency_member(contributions,members,members_list):
    for a in contributions:
        name_link=(a.find("b"))
        if name_link !=None :
            name=(name_link.text).replace(":",'')
            members[name]["frequency"]+=1
            pro_link=name_link.find("a")
        img_link=((pro_link['onmouseover'].replace("popup('<img src=\"","")).replace(" \" />');",""))
        members[name]["image"]=img_link
    return members

def check_for_name(contribution):
    name_link=(contribution.find("b"))
    if name_link !=None :
        return True
    else:
        return False

def get_raw_contributions(contributions):
    raw_contributions=[]

    cont_index=0
    i=0
    while(cont_index<len(contributions)):
        current_cont=contributions[cont_index]
        raw_dialog=""
        check=True
        temp_index=cont_index+1
        try:
            while(check_for_name(contributions[temp_index])==False):
                raw_dialog+=f" {contributions[temp_index].text}"
                temp_index+=1
                check=False

        except Exception as e:
            print(e)
        name_link1=(current_cont.find("b"))
        if name_link1 !=None :
            name1=(name_link1.text).replace(":",'')
            temp_data={
                "speaker":name1,
                "id":i,
                "dialog":f"{(current_cont.text).replace(name_link1.text,'').strip()} {raw_dialog}",
            }
            raw_contributions.append(temp_data)
            i+=1
        cont_index=temp_index
    return raw_contributions

def get_next_speaker(index,contributions):
    for x in range(index+1,len(contributions)):
        name_link=(contributions[x].find("b"))
        if name_link !=None :
            name=(name_link.text).replace(":",'')
            return name

def tokenizeIntoSentences(corpus):
    text =corpus   
    doc = nlp(text)
    spacyTokens= list(doc.sents)
    sent=[]
    for token in spacyTokens:
        sent.append(str(token))
    return sent

def get_main_data(contributions,members_list,members):
    current_member=""
    contribution_index=0
    for contribution in contributions:
        # ORGANIZING DIALOGS
        name_link=(contribution.find("b"))
        if name_link!=None:
            name=(name_link.text).replace(":","")
            if name in members_list:
                current_member=name
                count=1
                dialog_info={
                    "dialog":(contribution.text).replace(name_link.text,"").strip(),
                    "Asked":[],
                    "info":[]
                }
                # EXTRACTING QUESTIONS
                dialog=(contribution.text).replace(name_link.text,"").strip()
                sen_toks=tokenizeIntoSentences(dialog)
                for tok in sen_toks:
                    if"?" in tok:
                        ques_data={
                            "Question":tok,
                            "to":"",
                            "Confidence":""
                        }
                        ques_data["to"]=get_next_speaker(contribution_index,contributions)
                        if " you " in tok.lower() or " minister " in dialog.lower() or " she " in tok.lower() or " he " in tok.lower():
                            ques_data["Confidence"]="High"
                        else:
                            ques_data["Confidence"]="Low"
                        dialog_info["Asked"].append(ques_data)

                members[name]["dialogs"].append(dialog_info)
        else:
            dialog_info={
                    "dialog":(contribution.text).strip(),
                    "Asked":[],
                    "info":[]
                }
            # EXTRACTING QUESTIONS
            dialog=(contribution.text).strip()
            sen_toks=tokenizeIntoSentences(dialog)
            for tok in sen_toks:
                if"?" in tok:
                    ques_data={
                        "Question":tok,
                        "to":"",
                        "Confidence":""
                    }
                    ques_data["to"]=get_next_speaker(contribution_index,contributions)
                    if " you " in tok.lower() or " minister " in dialog.lower() or " she " in tok.lower() or " he " in tok.lower():
                        ques_data["Confidence"]="High"
                    else:
                        ques_data["Confidence"]="Low"
                    dialog_info["Asked"].append(ques_data)
            members[current_member]["dialogs"].append(dialog_info)
        contribution_index+=1
    return members

def get_spacy_data(members_list,members):
    for member in members_list:
        dialog_infos=members[member]["dialogs"]
        print(member)
        count=0
        for dialog_info in dialog_infos:
            sentences=tokenizeIntoSentences(dialog_info["dialog"])
            collection={'Member_mentions':[],
        'DATE':[], 'CARDINAL':[], 'ORG':[], 'WORK_OF_ART':[], 'TIME':[], 'PERSON':[], 'NORP':[], 'GPE':[], 'ORDINAL':[], 'FAC':[], 'LOC':[], 'MONEY':[], 'PERCENT':[], 'EVENT':[], 'PRODUCT':[], 'QUANTITY':[], 'LAW':[], 'LANGUAGE':[]
    }
            x=0
            for sentence in sentences:
                sent_spacy=nlp1(sentence)
                for ent in sent_spacy.ents:
                        if x==0:
                            ent_col={"Entity":ent.text,
                            "Sentence":f"{sentence.strip()}"}
                        elif x==1:
                            ent_col={"Entity":ent.text,
                            "Sentence":f"{sentences[0].strip()} {sentence.strip()}"}
                        else:
                            ent_col={"Entity":ent.text,
                            "Sentence":f"{sentences[x-2].strip()} {sentences[x-1].strip()} {sentence.strip()}"}
                        collection[ent.label_].append(ent_col)
                        try:
                            if ent.label_=="PERSON":
                                temp=(ent.text)
                                temp2=temp.split(" ")[1]
                                for member1 in members_list:
                                    if temp2 in member1:
                                        collection["Member_mentions"].append(member1)
                        except Exception as e:
                            pass

                                
                x+=1

            members[member]["dialogs"][count]["info"]=collection
            count+=1
    return members

if __name__ == "__main__":
    argv = sys.argv
    link = argv[1]
    page = requests.get(link)
    soup = BeautifulSoup(page.content, 'html.parser')
    contributions = soup.find_all("div", {"class": "Contribution"})
    # All Member Name
    out=get_all_members(contributions)
    members=out["skeleton"]
    members_list=out["list"]

    # Speech Frequency
    members=get_frequency_member(contributions,members,members_list)
    # Raw Contriutions
    raw_contributions=get_raw_contributions(contributions)
    # Get Main Data
    members=get_main_data(contributions,members_list,members)
    # Add spacy Data
    members=get_spacy_data(members_list,members)
    # Final Data
    final_data={
    "Members":members_list,
    "Speech Data":members
    }
    with open("insights.json","w") as outfile:
        json.dump(final_data, outfile,indent=4)  

with open("raw_contributions.json","w") as outfile:
    json.dump(raw_contributions, outfile,indent=4)  