import re
import sys
import logging
import spacy
from spacy.lang.en import English
from dask.threaded import get

from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.aspect import AnnotatedTextAspect
from ltldoorstep.reports.report import combine_reports
from ltldoorstep.document_utils import load_text, split_into_paragraphs
import unicodedata
import json

# Loading Spacy Models
try:
    nlp = English()
    nlp.add_pipe('sentencizer')
    nlp1 = spacy.load("en_core_web_lg")
except:
    print("FAILED TO LOAD THE SPACY MODEL!")

# Tokenizes the text into sentences
def tokenizeIntoSentences(corpus):
    text =corpus   
    doc = nlp(text)
    spacyTokens= list(doc.sents)
    sent=[]
    for token in spacyTokens:
        sent.append(str(token))
    return sent

# returns distinct values of list
def get_distinct_elements(list1):
    list2=[]
    for x in list1:
        if x not in list2:
            list2.append(x)
    return list2


"""
This finder extracts dates from the given text. Irrespective of the format. Additionally I tags all the findings sepcific to their content.Example:
1. for "19 January" tags would be ["month"]
2. for "19 January Wednesday" tags would be ["day","month"]
3. for "19 Today Wednesday" tags would be ["day","miscellaneous"]
"""
def date_finder(text, rprt):
    # Replacing some special characters causing unicode errors
    text=text.replace("£","GBP ")
    text=text.replace("·",".")

    # Toknizing the text into sentences
    sent_toks=tokenizeIntoSentences(text)
    sent_ents=[]

    # Extarcting all the possible dates from the text
    for tok in sent_toks:
        sent_spacy=nlp1(tok)
        for ent in sent_spacy.ents:
            if ent.label_=="DATE":
                sent_ents.append(ent.text)
    
    sent_ents=get_distinct_elements(sent_ents)

    # Spliting text into paragraphs
    paragraphs = split_into_paragraphs(text)

    # This is the counter for dates
    date_counts = {date: 0 for date in sent_ents}

    # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
    for para_n, (paragraph, line_number) in enumerate(paragraphs):

        paragraph_lower = paragraph.lower()

        for date in sent_ents:
            date_occurrences = paragraph_lower.count(date.lower())
            if date_occurrences >0:
                date_counts[date] += date_occurrences

                content = AnnotatedTextAspect(paragraph)

                # Lists of strings for very specific tagging
                months=["january","february","march","april","may","june","july","august","september","october","november","december","jan","feb","mar","apr","jun","jul","aug","sept","oct","nov","dec"]
                days=["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
                misc=["year","week","day","today","tomorrow","yesterday"]


                date_toks=(date.lower()).split(" ")
                for match in re.finditer(date.lower(), paragraph_lower):
                    match_tags=[]
                    # Applying Tags according to the content of the dates
                    if any(item in date_toks for item in months):
                        match_tags.append("month")
                    if any(item in date_toks for item in days):
                        match_tags.append("day")
                    if any(item in date_toks for item in misc):
                        match_tags.append("miscellaneous")
                    content.add(
                        note=f'Occurence of {date}',
                        start_offset=match.start(),
                        end_offset=match.end(),
                        level=logging.INFO,
                        tags=match_tags
                    )


                # Just to bring in it the detailed section of output.html
                if any(item in date_toks for item in months) or any(item in date_toks for item in days) or any(item in date_toks for item in misc):
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO

                rprt.add_issue(
                    urgency,
                    'date-cropped-up',
                    f'Found {date}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    for date, total in date_counts.items():
        rprt.add_issue(
            logging.INFO,
            'date-totals',
            f'Found {total} occurrences of {date}'
        )

    return rprt

"""
This finder extracts Monetary Values along with any adjectives of the value (if any).

WARNING!
sometimes use of curreny symbol may cause problem becuase of unicode.
"""
def money_finder(text, rprt):
    # Replacing some special characters causing unicode errors
    text=text.replace("£","GBP ")
    text=text.replace("€","EUR")
    text=text.replace("·",".")

    # Toknizing the text into sentences
    sent_toks=tokenizeIntoSentences(text)
    sent_ents=[]
    
    # Extarcting all the possible dates from the text
    for tok in sent_toks:
        sent_spacy=nlp1(tok)
        for ent in sent_spacy.ents:
            if ent.label_=="MONEY":
                sent_ents.append(ent.text)
    
    sent_ents=get_distinct_elements(sent_ents)
    # Spliting text into paragraphs
    paragraphs = split_into_paragraphs(text)

    # This is the counter for monetary values
    money_counts = {money: 0 for money in sent_ents}

    # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
    for para_n, (paragraph, line_number) in enumerate(paragraphs):

        paragraph_lower = paragraph.lower()

        for money in sent_ents:
            money_occurrences = paragraph_lower.count(money.lower())
            if money_occurrences >0:
                money_counts[money] += money_occurrences

                content = AnnotatedTextAspect(paragraph)
                for match in re.finditer(money.lower(), paragraph_lower):
                    content.add(
                        note=f'Occurence of {money}',
                        start_offset=match.start(),
                        end_offset=match.end(),
                        level=logging.INFO,
                        tags=["money"]
                    )


                if True:
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO

                rprt.add_issue(
                    urgency,
                    'money-cropped-up',
                    f'Found {money}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    for money, total in money_counts.items():
        rprt.add_issue(
            logging.INFO,
            'money-totals',
            f'Found {total} occurrences of {money}'
        )

    return rprt

"""
This finder extarcts quantitative values for example:100 Km, 200 miles
"""
def quantity_finder(text, rprt):
    # Replacing some special characters causing unicode errors
    text=text.replace("£","GBP ")
    text=text.replace("·",".")
    # Toknizing the text into sentences
    sent_toks=tokenizeIntoSentences(text)
    sent_ents=[]

    # Extarcting all the possible quantitative values from the text
    for tok in sent_toks:
        sent_spacy=nlp1(tok)
        for ent in sent_spacy.ents:
            if ent.label_=="QUANTITY":
                sent_ents.append(ent.text)


    sent_ents=get_distinct_elements(sent_ents)

    # Spliting text into paragraphs
    paragraphs = split_into_paragraphs(text)

    # This is the counter for quantitative values
    quantity_counts = {quantity: 0 for quantity in sent_ents}

    # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
    for para_n, (paragraph, line_number) in enumerate(paragraphs):

        paragraph_lower = paragraph.lower()

        for quantity in sent_ents:
            quantity_occurrences = paragraph_lower.count(quantity.lower())
            if quantity_occurrences >0:
                quantity_counts[quantity] += quantity_occurrences

                content = AnnotatedTextAspect(paragraph)
                for match in re.finditer(quantity.lower(), paragraph_lower):
                    content.add(
                        note=f'Occurence of {quantity}',
                        start_offset=match.start(),
                        end_offset=match.end(),
                        level=logging.INFO,
                        tags=["quantity"]
                    )


                if True:
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO

                rprt.add_issue(
                    urgency,
                    'quantity-cropped-up',
                    f'Found {quantity}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    for quantity, total in quantity_counts.items():
        rprt.add_issue(
            logging.INFO,
            'quantity-totals',
            f'Found {total} occurrences of {quantity}'
        )

    return rprt

"""
This finder extracts Percentage Values along with any adjectives of the value (if any).

"""
def percent_finder(text, rprt):
    # Replacing some special characters causing unicode errors
    text=text.replace("£","GBP ")
    text=text.replace("·",".")

    # Toknizing the text into sentences
    sent_toks=tokenizeIntoSentences(text)
    sent_ents=[]

    # Extarcting all the possible dates from the text
    for tok in sent_toks:
        sent_spacy=nlp1(tok)
        for ent in sent_spacy.ents:
            if ent.label_=="PERCENT":
                sent_ents.append(ent.text)
    
    sent_ents=get_distinct_elements(sent_ents)

    # Spliting text into paragraphs
    paragraphs = split_into_paragraphs(text)

    # This is the counter for percentage Values
    percent_counts = {percent: 0 for percent in sent_ents}

    # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
    for para_n, (paragraph, line_number) in enumerate(paragraphs):

        paragraph_lower = paragraph.lower()

        for percent in sent_ents:
            percent_occurrences = paragraph_lower.count(percent.lower())
            if percent_occurrences >0:
                percent_counts[percent] += percent_occurrences

                content = AnnotatedTextAspect(paragraph)
                for match in re.finditer(percent.lower(), paragraph_lower):
                    content.add(
                        note=f'Occurence of {percent}',
                        start_offset=match.start(),
                        end_offset=match.end(),
                        level=logging.INFO,
                        tags=["percentage"]
                    )


                if True:
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO

                rprt.add_issue(
                    urgency,
                    'percent-cropped-up',
                    f'Found {percent}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    for person, total in percent_counts.items():
        rprt.add_issue(
            logging.INFO,
            'percent-totals',
            f'Found {total} occurrences of {percent}'
        )

    return rprt

"""
This finder extracts all the questions from the text
"""
def question_finder(text, rprt):
    # Replacing some special characters causing unicode errors
    text=text.replace("£","GBP ")
    text=text.replace("·",".")
    sent_toks=tokenizeIntoSentences(text)
    sent_ents=[]

    # Extacting all the questions from the text
    for tok in sent_toks:
        if "?" in tok:
            sent_ents.append(tok)
    
    sent_ents=get_distinct_elements(sent_ents)

    # Spliting text into paragraphs
    paragraphs = split_into_paragraphs(text)

    # This is the counter for questions
    question_counts = {question: 0 for question in sent_ents}

    # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
    for para_n, (paragraph, line_number) in enumerate(paragraphs):

        paragraph_lower = paragraph.lower()

        for question in sent_ents:
            question_occurrences = paragraph_lower.count(question.lower())
            if question_occurrences >0:
                question_counts[question] += question_occurrences

                content = AnnotatedTextAspect(paragraph)
                # Checking if there is a question in the given paragraph
                if question.lower() in paragraph_lower:
                    content.add(
                        note=f'Occurence of {question}',
                        start_offset=paragraph.find(question),
                        end_offset=paragraph.find(question)+len(question),
                        level=logging.INFO,
                        tags=["question"]
                    )
            

                if True:
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO

                rprt.add_issue(
                    urgency,
                    'question-cropped-up',
                    f'Found {question}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    for person, total in question_counts.items():
        rprt.add_issue(
            logging.INFO,
            'question-totals',
            f'Found {total} occurrences of {question}'
        )

    return rprt

"""
This finder extarcts all the geographical locations
"""
def location_finder(text, rprt):
    # Replacing some special characters causing unicode errors
    text=text.replace("£","GBP ")
    text=text.replace("·",".")

    # Toknizing the text into sentences
    sent_toks=tokenizeIntoSentences(text)
    sent_ents=[]

    # Extarcting all the possible dates from the text
    for tok in sent_toks:
        sent_spacy=nlp1(tok)
        for ent in sent_spacy.ents:
            if ent.label_=="GPE":
                sent_ents.append(ent.text)
    
    sent_ents=get_distinct_elements(sent_ents)

    # Spliting text into paragraphs
    paragraphs = split_into_paragraphs(text)

    # This is the counter for locations
    location_counts = {location: 0 for location in sent_ents}

    # Now we loop through the paragraphs - `enumerate` gives us a running count in `para_n`
    for para_n, (paragraph, line_number) in enumerate(paragraphs):

        paragraph_lower = paragraph.lower()

        for location in sent_ents:
            location_occurrences = paragraph_lower.count(location.lower())
            if location_occurrences >0:
                location_counts[location] += location_occurrences

                content = AnnotatedTextAspect(paragraph)
                for match in re.finditer(location.lower(), paragraph_lower):
                    content.add(
                        note=f'Occurence of {location}',
                        start_offset=match.start(),
                        end_offset=match.end(),
                        level=logging.INFO,
                        tags=["location"]
                    )


                if True:
                    urgency = logging.WARNING
                else:
                    urgency = logging.INFO

                rprt.add_issue(
                    urgency,
                    'location-cropped-up',
                    f'Found {location}',
                    line_number=line_number,
                    character_number=0,
                    content=content
                )

    for person, total in location_counts.items():
        rprt.add_issue(
            logging.INFO,
            'location-totals',
            f'Found {total} occurrences of {location}'
        )

    return rprt


class CityFinderProcessor(DoorstepProcessor):

    preset = 'document'

    code = 'lintol-code-challenge-general-processor'

    description = "City Finder for Lintol Coding Challenge"


    def get_workflow(self, filename, metadata={}):
        print("processing...\nThis is may take few minutes")
        workflow = {
            'load-text': (load_text, filename),
            'get-report': (self.make_report,),
            'step-A': (date_finder, 'load-text', 'get-report'),
            'step-B': (money_finder, 'load-text', 'get-report'),
            'step-C': (percent_finder, 'load-text', 'get-report'),
            'step-D': (quantity_finder, 'load-text', 'get-report'),
            'step-E': (question_finder, 'load-text', 'get-report'),
            'step-F': (location_finder, 'load-text', 'get-report'),
            'output': (workflow_condense, 'step-A', 'step-B', 'step-C','step-D','step-E','step-F')
        }
        return workflow


def workflow_condense(base, *args):
    return combine_reports(*args, base=base)

processor = CityFinderProcessor.make


if __name__ == "__main__":
    argv = sys.argv
    processor = CityFinderProcessor()
    processor.initialize()
    workflow = processor.build_workflow(argv[1])
    output=(get(workflow, 'output'))
    print(output)
