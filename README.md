# Lintol Challenge

Processor for Data extraction


Flask_server : It contains a web app that showcases the insights through a UI.

Processor.py:
 Extracts Following :

 1. Dates
 2. Quantity
 3. Locations
 4. Monetary Values
 5. Questions
 6. Percentage Values

insights.py:
It gives you following insights about the speakers:

1. Questions they asked to whom
2. Frequency of when spoke
3. Amount they spoke
4. Sequence of speakers
